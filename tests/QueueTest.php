<?php

use Astartsky\MessageQueue\QueueException;
use Astartsky\MessageQueue\SQSQueue;
use Psr\Log\Test\LoggerInterfaceTest;

class QueueTest extends \PHPUnit_Framework_TestCase
{
    public function testQueueCreate()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->getMock();

        $sqsOptions = [
            'url' => 'SQSURL',
            'max_message_size' => 256,
            'time_limit' => 5,
            'message_number_limit' => 5
        ];

        new SQSQueue($sqsClient, $sqsOptions);
    }

    public function testQueueCreateMissingUrl()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->getMock();

        $sqsOptions = [
            'max_message_size' => 256,
            'time_limit' => 5,
            'message_number_limit' => 5
        ];

        $this->setExpectedException('\InvalidArgumentException', 'The queue option "url" must be provided.');

        new SQSQueue($sqsClient, $sqsOptions);
    }

    public function testQueueCreateUrlIsNotAString()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->getMock();

        $sqsOptions = [
            'url' => 1,
            'max_message_size' => 256,
            'time_limit' => 5,
            'message_number_limit' => 5
        ];

        $this->setExpectedException('\InvalidArgumentException', 'The queue option "url" must be a string.');

        new SQSQueue($sqsClient, $sqsOptions);
    }

    public function testQueueCreateMaxMessageSizeIsNotANumber()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->getMock();

        $sqsOptions = [
            'url' => 'SQSURL',
            'max_message_size' => "big",
            'time_limit' => 5,
            'message_number_limit' => 5
        ];

        $this->setExpectedException('\InvalidArgumentException', 'The queue option "max_message_size" must be a number.');

        new SQSQueue($sqsClient, $sqsOptions);
    }

    public function testQueueCreateTimeLimitIsNotANumber()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->getMock();

        $sqsOptions = [
            'url' => 'SQSURL',
            'max_message_size' => 256,
            'time_limit' => "two hours",
            'message_number_limit' => 5
        ];

        $this->setExpectedException('\InvalidArgumentException', 'The queue option "time_limit" must be a number.');

        new SQSQueue($sqsClient, $sqsOptions);
    }

    public function testQueueCreateMessageNumberLimitIsNotANumber()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->getMock();

        $sqsOptions = [
            'url' => 'SQSURL',
            'max_message_size' => 256,
            'time_limit' => 5,
            'message_number_limit' => "two or three"
        ];

        $this->setExpectedException('\InvalidArgumentException', 'The queue option "message_number_limit" must be a number.');

        new SQSQueue($sqsClient, $sqsOptions);
    }

    protected $defaultOptions = [
        'url' => 'SQSURL',
        'max_message_size' => 256,
        'time_limit' => 5,
        'message_number_limit' => 4
    ];

    public function testQueuePushMessageSuccess()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['sendMessage'])
            ->getMock();

        $response = $this->getMockBuilder('\Guzzle\Service\Resource\Model')
            ->disableOriginalConstructor()
            ->setMethods(['getPath'])
            ->getMock();

        $response
            ->expects($this->once())
            ->method('getPath')
            ->with('ResponseMetadata/RequestId')
            ->willReturn('request-id-123');

        $message = "message";

        $sqsClient
            ->expects($this->once())
            ->method('sendMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'MessageBody' => $message
            ])
            ->will($this->returnValue($response));

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $requestId = $queue->push($message);

        $this->assertInternalType('string', $requestId);
    }

    public function testQueuePushBigMessage()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['sendMessage'])
            ->getMock();

        $message = "";
        do {
            $message .= "message";
        } while (strlen($message) <= 256 * 1024);

        $sqsClient
            ->expects($this->never())
            ->method('sendMessage');

        $this->setExpectedException('\Astartsky\MessageQueue\QueueException', 'Message size is above limit.');

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $queue->push($message);
    }

    public function testQueuePushMessageSQSFailure()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['sendMessage'])
            ->getMock();

        $message = "message";

        $sqsClient
            ->expects($this->once())
            ->method('sendMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'MessageBody' => $message
            ])
            ->willThrowException(new QueueException('Push to queue failed.'));

        $this->setExpectedException('\Astartsky\MessageQueue\QueueException', 'Push to queue failed.');

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $queue->push($message);
    }

    public function testQueuePushMessageNotString()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['sendMessage'])
            ->getMock();

        $message = 42;

        $sqsClient
            ->expects($this->never())
            ->method('sendMessage');

        $this->setExpectedException('\InvalidArgumentException', 'The message must be a string.');

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $queue->push($message);
    }

    public function testQueueNoMessages()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['receiveMessage', 'deleteMessage'])
            ->getMock();

        $response = $this->getMockBuilder('\Guzzle\Service\Resource\Model')
            ->disableOriginalConstructor()
            ->setMethods(['getPath'])
            ->getMock();

        $response
            ->expects($this->any())
            ->method('getPath')
            ->with('Messages')
            ->willReturn([]);

        $sqsClient
            ->expects($this->once())
            ->method('receiveMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'MaxNumberOfMessages' => $this->defaultOptions['message_number_limit']
            ])
            ->will($this->returnValue($response));

        $sqsClient
            ->expects($this->never())
            ->method('deleteMessage');

        $worker = $this->getMockBuilder('\Astartsky\MessageQueue\WorkerInterface')
            ->setMethods(['work'])
            ->getMock();

        $worker->expects($this->never())
            ->method('work');

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $queue->work($worker);
    }

    public function testQueueOneMessage()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['receiveMessage', 'deleteMessage'])
            ->getMock();

        $response = $this->getMockBuilder('\Guzzle\Service\Resource\Model')
            ->disableOriginalConstructor()
            ->setMethods(['getPath'])
            ->getMock();

        $response
            ->expects($this->any())
            ->method('getPath')
            ->with('Messages')
            ->willReturn([
                [
                    'Body' => 'message_body',
                    'ReceiptHandle' => 'message_id'
                ]
            ]);

        $sqsClient
            ->expects($this->once())
            ->method('receiveMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'MaxNumberOfMessages' => $this->defaultOptions['message_number_limit']
            ])
            ->will($this->returnValue($response));

        $sqsClient
            ->expects($this->once())
            ->method('deleteMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'ReceiptHandle' => 'message_id'
            ]);

        $worker = $this->getMockBuilder('\Astartsky\MessageQueue\WorkerInterface')
            ->setMethods(['work'])
            ->getMock();

        $worker
            ->expects($this->once())
            ->method('work')
            ->with('message_body')
            ->willReturn(true);

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $queue->work($worker);
    }

    public function testQueueOneMessageWorkerFailed()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['receiveMessage', 'deleteMessage'])
            ->getMock();

        $response = $this->getMockBuilder('\Guzzle\Service\Resource\Model')
            ->disableOriginalConstructor()
            ->setMethods(['getPath'])
            ->getMock();

        $response
            ->expects($this->any())
            ->method('getPath')
            ->with('Messages')
            ->willReturn([
                [
                    'Body' => 'message_body',
                    'ReceiptHandle' => 'message_id'
                ]
            ]);

        $sqsClient
            ->expects($this->once())
            ->method('receiveMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'MaxNumberOfMessages' => $this->defaultOptions['message_number_limit']
            ])
            ->will($this->returnValue($response));

        $sqsClient
            ->expects($this->never())
            ->method('deleteMessage');

        $worker = $this->getMockBuilder('\Astartsky\MessageQueue\WorkerInterface')
            ->setMethods(['work'])
            ->getMock();

        $worker
            ->expects($this->once())
            ->method('work')
            ->with('message_body')
            ->willReturn(false);

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $queue->work($worker);
    }

    public function testQueueMultipleMessages()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['receiveMessage', 'deleteMessage'])
            ->getMock();

        $response = $this->getMockBuilder('\Guzzle\Service\Resource\Model')
            ->disableOriginalConstructor()
            ->setMethods(['getPath'])
            ->getMock();

        $response
            ->expects($this->any())
            ->method('getPath')
            ->with('Messages')
            ->willReturn([
                [
                    'Body' => 'message_body1',
                    'ReceiptHandle' => 'message_id1'
                ],
                [
                    'Body' => 'message_body2',
                    'ReceiptHandle' => 'message_id2'
                ]
            ]);

        $sqsClient
            ->expects($this->once())
            ->method('receiveMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'MaxNumberOfMessages' => $this->defaultOptions['message_number_limit']
            ])
            ->will($this->returnValue($response));

        $sqsClient
            ->expects($this->at(1))
            ->method('deleteMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'ReceiptHandle' => 'message_id1'
            ]);

        $sqsClient
            ->expects($this->at(2))
            ->method('deleteMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'ReceiptHandle' => 'message_id2'
            ]);

        $worker = $this->getMockBuilder('\Astartsky\MessageQueue\WorkerInterface')
            ->setMethods(['work'])
            ->getMock();

        $worker
            ->expects($this->at(0))
            ->method('work')
            ->with('message_body1')
            ->willReturn(true);

        $worker
            ->expects($this->at(1))
            ->method('work')
            ->with('message_body2')
            ->willReturn(true);

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $queue->work($worker);
    }

    public function testQueueMultipleMessagesOneFailed()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['receiveMessage', 'deleteMessage'])
            ->getMock();

        $response = $this->getMockBuilder('\Guzzle\Service\Resource\Model')
            ->disableOriginalConstructor()
            ->setMethods(['getPath'])
            ->getMock();

        $response
            ->expects($this->any())
            ->method('getPath')
            ->with('Messages')
            ->willReturn([
                [
                    'Body' => 'message_body1',
                    'ReceiptHandle' => 'message_id1'
                ],
                [
                    'Body' => 'message_body2',
                    'ReceiptHandle' => 'message_id2'
                ]
            ]);

        $sqsClient
            ->expects($this->once())
            ->method('receiveMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'MaxNumberOfMessages' => $this->defaultOptions['message_number_limit']
            ])
            ->will($this->returnValue($response));

        $sqsClient
            ->expects($this->once())
            ->method('deleteMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'ReceiptHandle' => 'message_id1'
            ]);

        $worker = $this->getMockBuilder('\Astartsky\MessageQueue\WorkerInterface')
            ->setMethods(['work'])
            ->getMock();

        $worker
            ->expects($this->at(0))
            ->method('work')
            ->with('message_body1')
            ->willReturn(true);

        $worker
            ->expects($this->at(1))
            ->method('work')
            ->with('message_body2')
            ->willReturn(false);

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $queue->work($worker);
    }

    public function testQueueMultipleMessagesAllFailed()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['receiveMessage', 'deleteMessage'])
            ->getMock();

        $response = $this->getMockBuilder('\Guzzle\Service\Resource\Model')
            ->disableOriginalConstructor()
            ->setMethods(['getPath'])
            ->getMock();

        $response
            ->expects($this->any())
            ->method('getPath')
            ->with('Messages')
            ->willReturn([
                [
                    'Body' => 'message_body1',
                    'ReceiptHandle' => 'message_id1'
                ],
                [
                    'Body' => 'message_body2',
                    'ReceiptHandle' => 'message_id2'
                ]
            ]);

        $sqsClient
            ->expects($this->once())
            ->method('receiveMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'MaxNumberOfMessages' => $this->defaultOptions['message_number_limit']
            ])
            ->will($this->returnValue($response));

        $sqsClient
            ->expects($this->never())
            ->method('deleteMessage');

        $worker = $this->getMockBuilder('\Astartsky\MessageQueue\WorkerInterface')
            ->setMethods(['work'])
            ->getMock();

        $worker
            ->expects($this->at(0))
            ->method('work')
            ->with('message_body1')
            ->willReturn(false);

        $worker
            ->expects($this->at(1))
            ->method('work')
            ->with('message_body2')
            ->willReturn(false);

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $queue->work($worker);
    }

    public function testQueueReceiveFailed()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['receiveMessage', 'deleteMessage'])
            ->getMock();

        $sqsClient
            ->expects($this->once())
            ->method('receiveMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'MaxNumberOfMessages' => $this->defaultOptions['message_number_limit']
            ])
            ->willThrowException(new \Exception());

        $sqsClient
            ->expects($this->never())
            ->method('deleteMessage');

        $worker = $this->getMockBuilder('\Astartsky\MessageQueue\WorkerInterface')
            ->setMethods(['work'])
            ->getMock();

        $worker
            ->expects($this->never())
            ->method('work');

        $this->setExpectedException('\Astartsky\MessageQueue\QueueException', 'Pull from the queue failed.');

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $queue->work($worker);
    }

    public function testQueueMultipleMessagesDeleteFailed()
    {
        $sqsClient = $this->getMockBuilder('\Aws\Sqs\SqsClient')
            ->disableOriginalConstructor()
            ->setMethods(['receiveMessage', 'deleteMessage'])
            ->getMock();

        $response = $this->getMockBuilder('\Guzzle\Service\Resource\Model')
            ->disableOriginalConstructor()
            ->setMethods(['getPath'])
            ->getMock();

        $response
            ->expects($this->any())
            ->method('getPath')
            ->with('Messages')
            ->willReturn([
                [
                    'Body' => 'message_body',
                    'ReceiptHandle' => 'message_id'
                ]
            ]);

        $sqsClient
            ->expects($this->once())
            ->method('receiveMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'MaxNumberOfMessages' => $this->defaultOptions['message_number_limit']
            ])
            ->will($this->returnValue($response));

        $sqsClient
            ->expects($this->once())
            ->method('deleteMessage')
            ->with([
                'QueueUrl' => $this->defaultOptions['url'],
                'ReceiptHandle' => 'message_id'
            ])
            ->willThrowException(new \Exception());

        $worker = $this->getMockBuilder('\Astartsky\MessageQueue\WorkerInterface')
            ->setMethods(['work'])
            ->getMock();

        $worker
            ->expects($this->at(0))
            ->method('work')
            ->with('message_body')
            ->willReturn(true);

        $this->setExpectedException('\Astartsky\MessageQueue\QueueException', 'Remove finished message from the queue failed.');

        $queue = new SQSQueue($sqsClient, $this->defaultOptions);
        $queue->work($worker);
    }
}