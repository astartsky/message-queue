<?php
namespace Astartsky\MessageQueue;

interface QueueInterface
{
    /**
     * @param array|object$message
     */
    public function push($message);

    /**
     * @param WorkerInterface $worker
     */
    public function work(WorkerInterface $worker);
}