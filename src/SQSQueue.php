<?php
namespace Astartsky\MessageQueue;

use Aws\Sqs\SqsClient;
use Guzzle\Service\Resource\Model;

class SQSQueue implements QueueInterface
{
    protected $sqsClient;

    protected $queueUrl;
    protected $maxMessageSize;
    protected $timeLimit;
    protected $messageNumberLimit;

    /**
     * SQSQueue constructor.
     * @param SqsClient $sqsClient
     * @param $queueOptions
     */
    public function __construct(SqsClient $sqsClient, $queueOptions)
    {
        $this->sqsClient = $sqsClient;

        foreach (array('url', 'max_message_size', 'time_limit', 'message_number_limit') as $key) {
            if (!isset($queueOptions[$key])) {
                throw new \InvalidArgumentException(sprintf('The queue option "%s" must be provided.', $key));
            }
        }

        if (false === is_string($queueOptions['url'])) {
            throw new \InvalidArgumentException('The queue option "url" must be a string.');
        }

        if (false === is_numeric($queueOptions['max_message_size'])) {
            throw new \InvalidArgumentException('The queue option "max_message_size" must be a number.');
        }

        if (false === is_numeric($queueOptions['time_limit'])) {
            throw new \InvalidArgumentException('The queue option "time_limit" must be a number.');
        }

        if (false === is_numeric($queueOptions['message_number_limit'])) {
            throw new \InvalidArgumentException('The queue option "message_number_limit" must be a number.');
        }

        $this->queueUrl = $queueOptions['url'];
        $this->maxMessageSize = $queueOptions['max_message_size'];
        $this->timeLimit = $queueOptions['time_limit'];
        $this->messageNumberLimit = $queueOptions['message_number_limit'];
    }

    /**
     * @param string $message
     * @return string
     * @throws QueueException
     */
    public function push($message)
    {
        if (false === is_string($message)) {
            throw new \InvalidArgumentException('The message must be a string.');
        }

        $size = strlen($message);
        if ($size > $this->maxMessageSize * 1024) {
            throw new QueueException("Message size is above limit.", 0);
        }

        try {
            /** @var Model $response */
            $response = $this->sqsClient->sendMessage(array(
                'QueueUrl' => $this->queueUrl,
                'MessageBody' => $message
            ));

        } catch (\Exception $e) {
            throw new QueueException("Push to queue failed.", 0, $e);
        }

        return $response->getPath('ResponseMetadata/RequestId');
    }

    /**
     * @param WorkerInterface $worker
     * @throws QueueException
     */
    public function work(WorkerInterface $worker)
    {
        $count = 0;
        $time = time();

        try {
            /** @var Model $result */
            $result = $this->sqsClient->receiveMessage(
                array(
                    'QueueUrl' => $this->queueUrl,
                    'MaxNumberOfMessages' => $this->messageNumberLimit
                )
            );
        } catch (\Exception $e) {
            throw new QueueException("Pull from the queue failed.", 0, $e);
        }

        if ($result->getPath('Messages')) {

            /** @var Model $message */
            foreach ($result->getPath('Messages') as $messageArray) {

                $count += 1;
                if ($worker->work($messageArray['Body'])) {
                    try {
                        $this->sqsClient->deleteMessage(array(
                            'QueueUrl' => $this->queueUrl,
                            'ReceiptHandle' => (string) $messageArray['ReceiptHandle']
                        ));
                    } catch (\Exception $e) {
                        throw new QueueException("Remove finished message from the queue failed.", 0, $e);
                    }
                }

                if ((time() - $time) >= $this->timeLimit) {
                    break;
                }
            }
        }
    }
}