<?php
namespace Astartsky\MessageQueue;

interface WorkerInterface
{
    /**
     * @param array $message
     * @return bool
     */
    public function work($message);
}